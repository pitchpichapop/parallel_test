import os
import traceback
import zc.lockfile
import random
import openpyxl
import envs
import excels_utils
import math

def num2sting(v) :
    if v > 0 :
        return '+'+str(v)
    else :
        return str(v)

def mkdir(d):
    if not os.path.exists(d):
        try:
            os.makedirs(d)
        except FileExistsError:
            print(traceback.format_exc())

def zc_lockfile(kw):
    # os.chdir(os.path.dirname(__file__))
    lockpath = os.path.join(os.path.dirname(__file__), 'locks')
    mkdir(lockpath)
    
    s = kw+'.lock'
    try:
        os.chdir(lockpath)
        # s = os.path.join(lockpath, s)
        print('locking4:',s)
        lock = zc.lockfile.LockFile(s)
        return lock
    except zc.lockfile.LockError:
        # raise
        raise Exception("Another %s job is still doing, please wait" % kw)
    return None

def random_choice(mylst) :
    ans,_,_,_ = mylst
    choice_shuffle = mylst
    random.shuffle(choice_shuffle)
    return choice_shuffle , choice_shuffle.index(ans)+1

all_test = []
def get_all_test(f_test) :
    wb = openpyxl.load_workbook(f_test,data_only=True)
    ws = wb['Sheet1']
    
    for i,row in enumerate(ws) :
        if i==0 :
            continue
        row = excels_utils.gettmpr(row)
        test = row[0],row[1],row[2],row[3],row[4],row[5],row[6]
        all_test.append(test)
    return all_test

def gen_item_1() :
    item1 = r'\item %s'%(all_test[0][1])
    value4random = {'random1' : ['3','-3:3',random.choice(['1-x^2','-1+x^2'])],
                    'random2' : ['5','-3:3',random.choice(['4-x^2','-4+x^2'])],
                    'random3' : ['10','-4:4',random.choice(['9-x^2','-9+x^2'])],
                    'random4' : ['20','-6:6',random.choice(['16-x^2','-16+x^2'])],
                    'random5' : ['30','-6:6',random.choice(['25-x^2','-25+x^2'])],
                    'random6' : ['40','-8:8',random.choice(['36-x^2','-36+x^2'])]
                    }
    x = random.choice([e for e in value4random.keys()])
    v = value4random[x]
    item1 += r'''
    \begin{center}
    \begin{tikzpicture}
    \begin{axis}[
        axis x line=middle, 
        axis y line=middle, 
        ymax=%s, ylabel=$y$, 
        xlabel=$x$
        ]
    \addplot[domain=%s, blue, smooth] {%s};
    \end{axis}
    \end{tikzpicture}
    \end{center}
    '''%(v[0],v[1],v[2])
 
    choice1 = '$%s$ และ $%s$'%(x[6],'-'+x[6])
    choice2 = '$%s$'%x[6]
    choice3 = '$0$ และ $%s$'%x[6]
    choice4 = '$0$ และ $%s$'%('-'+x[6])

    my_random,ans1 = random_choice([choice1,choice2,choice3,choice4])
    # print(my_random,ans)
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item1 += choice
 
    return item1,str(ans1)

def gen_item_2() :
    item2 = r'\item %s '%(all_test[1][1])
    sym1 = random.choice(['','-'])
    h = str(random.randint(1,9))
    k = random.choice(['+','-']) + str(random.randint(1,9))
    eq = r'$y=%s(x-%s)^2%s$'%(sym1,h,k)
    item2 += eq + '\n'
    if k.startswith('+') :
        k = k[1]
    h_false = str(-int(h))
    k_false = str(-int(k))
    choice1 = '$(%s,%s)$'%(h,k)
    choice2 = '$(%s,%s)$'%(h_false,k)
    choice3 = '$(%s,%s)$'%(h,k_false)
    choice4 = '$(%s,%s)$'%(h_false,k_false)
    
    my_random , ans2 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item2 += choice
    
    # print(item2 , str(ans2))
    return item2 , str(ans2)

def gen_item_3() :
    item3 = r'\item %s '%(all_test[2][1])
    has0ans = r'''
\begin{tikzpicture}
\begin{axis}[
    axis lines=middle,
    xmax=5,
    xmin=-5,
    ymin=%s,
    ymax=%s,
    xlabel={$x$},
    ylabel={$y$}
]
\addplot [domain=-5:5, samples=100,
          thick, blue] {%s};
\end{axis}
\end{tikzpicture}
    '''
    
    has1ans = r'''
\begin{tikzpicture}
\begin{axis}[
    axis lines=middle,
    xmax=5,
    xmin=-5,
    ymin=%s,
    ymax=%s,
    xlabel={$x$},
    ylabel={$y$}
]
\addplot [domain=-5:5, samples=100,
          thick, blue] {%s};
\end{axis}
\end{tikzpicture}
    '''
    
    has2ans_pos = r'''
\begin{tikzpicture}
\begin{axis}[
    axis lines=middle,
    xmax=5,
    xmin=-5,
    ymin=-2,
    ymax=5,
    xlabel={$x$},
    ylabel={$y$}
]
\addplot [domain=-5:5, samples=100,
          thick, blue] {(x%s2)^2-1};
\end{axis}
\end{tikzpicture}
    '''%random.choice(['+','-'])
    
    has2ans_neg = r'''
\begin{tikzpicture}
\begin{axis}[
    axis lines=middle,
    xmax=5,
    xmin=-5,
    ymin=-2,
    ymax=5,
    xlabel={$x$},
    ylabel={$y$}
]
\addplot [domain=-5:5, samples=100,
          thick, blue] {-(x%s1)^2+3};
\end{axis}
\end{tikzpicture}
    '''%random.choice(['+','-'])
    
    choice1 = random.choice([has0ans%('-2','5','x^2+1'),has0ans%('-5','2','-x^2-1')])
    choice2 = random.choice([has1ans%('-2','5','x^2'),has1ans%('-5','2','-x^2')])
    choice3 = has2ans_pos
    choice4 = has2ans_neg
    
    my_random , ans3 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item3 += choice
    
    return item3,str(ans3)

def gen_item_4() :
    item4 = r'\item %s '%(all_test[3][1])
    choice1 = '$(x%s%d)^2-%d=0$'%(random.choice(['+','-']),random.randint(1,50),random.randint(1,50))
    choice2 = '$%d(x%s%d)^2=0$'%(random.randint(1,50),random.choice(['+','-']),random.randint(1,55))
    choice3 = '$(x%s%d)^2+%d=0$'%(random.choice(['+','-']),random.randint(1,50),random.randint(1,50))
    choice4 = '$-(x%s%d)^2-%d=0$'%(random.choice(['+','-']),random.randint(1,50),random.randint(1,50))
    
    my_random , ans4 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item4 += choice
    
    return item4 , str(ans4)

def gen_item_5() :
    item5 = r'\item %s '%(all_test[4][1])
    choice1 = '$(x%s%d)^2+%d=0$'%(random.choice(['+','-']),random.randint(1,50),random.randint(1,50))
    choice2 = '$%d(x%s%d)^2=0$'%(random.randint(1,50),random.choice(['+','-']),random.randint(1,55))
    choice3 = '$(x%s%d)^2-%d=0$'%(random.choice(['+','-']),random.randint(1,50),random.randint(1,50))
    choice4 = '$-(x%s%d)^2-%d=0$'%(random.choice(['+','-']),random.randint(1,50),random.randint(1,50))
    
    my_random , ans5 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item5 += choice
    
    return item5 , str(ans5)

def gen_item_6() :
    item6 = r'\item %s '%(all_test[5][1])
    
    choice1 = '$y=-x^2-%d$'%(random.randint(1,10))**2
    choice2 = '$y=x^2-%d$'%(random.randint(1,10))**2
    choice3 = '$y=-x^2+%d$'%random.randint(1,9)
    choice4 = '$y=(x-%d)^2-%d$'%(random.randint(1,9),random.randint(1,9))
    
    my_random , ans6 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item6 += choice
    
    return item6 , str(ans6)

def gen_item_7() :
    item7 = '\item %s '%(all_test[6][1])
    a = 1
    b = random.choice([-2,2])*random.randint(1,4)
    c = random.choice([-1,1])*random.randint(1,9)

    b4eq = num2sting(b)
    c4eq = num2sting(c)
    # if b > 0 :
    #     b4eq = '+' + str(b)
    # else :
    #     b4eq = str(b)
    
    # if c > 0 :
    #     c4eq = '+' + str(c)
    # else :
    #     c4eq = str(c)
    
    item7 = item7%('$y=x^2%sx%s$'%(b4eq,c4eq))
    
    h = int(-b/(2*a))
    k = int(c - b**2/(4*a))
    
    choice1 = '$%s$'%str(a+h+k)
    choice2 = '$%s$'%str(a-h+k)
    choice3 = '$%s$'%str(a+h-k)
    choice4 = '$%s$'%str(a-h-k)
    
    my_random , ans7 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item7 += choice
    
    return item7, str(ans7)


def gen_item_8() :
    item8 = '\item %s '%(all_test[7][1])
    
    x = str(random.randint(1,9))
    choice1_v1 = '$y=%s-x-x^2$ จะได้กราฟพาราโบลาคว่ำ และจุดยอดอยู่ที่ $(0,%s)$'%(x,x)
    h = random.choice([-1,1])*random.randint(1,9)
    k = random.choice([-1,1])*random.randint(1,9)
    h4eq = num2sting(h)
    k4eq = num2sting(k)
    parabora_type_v1 = {'หงาย' : '>0' , 'คว่ำ' : '<0'}
    e = random.choice([type for type in parabora_type_v1.keys()])
    choice2_v1 = '$y=a(x%s)^2%s$ เมื่อ $a%s$ จะได้กราฟพาราโบลา%s จุดยอดอยู่ที่ $(%s,%s)$'%(h4eq,k4eq,parabora_type_v1[e],e,str(-h),str(k))
    
    h = random.choice([-1,1])*random.randint(1,9)
    k = random.choice([-1,1])*random.randint(1,9)
    h4eq = num2sting(h)
    k4eq = num2sting(k)
    parabora_type_v2 = {'หงาย' : '+' , 'คว่ำ' : '-'}
    ee = random.choice([type for type in parabora_type_v2.keys()])
    choice3_v1 = '$y=%d%s(x%s)^2$ จะได้กราฟ%s จุดยอดอยู่ที่ $(%s,%s)$'%(k,parabora_type_v2[ee],h4eq,ee,str(-h),str(k))
    
    choice4 = 'จุด %s เป็นจุดตัดหนึ่งของกราฟของฟังก์ชัน $y=x^2$ และ $y=x$'%random.choice(['$(0,0)$','$(1,1)$'])
    v1 = [choice1_v1,choice2_v1,choice3_v1,choice4]
    
    choice1_v2 = '$y=a(x%s)^2%s$ เมื่อ $a%s$ จะได้กราฟพาราโบลา%s จุดยอดอยู่ที่ $(%s,%s)$'%(h4eq,k4eq,parabora_type_v1[e],e,str(h),str(-k))
    choice2_v2 = '$y=%d%s(x%s)^2$ จะได้กราฟ%s จุดยอดอยู่ที่ $(%s,%s)$'%(k,parabora_type_v2[ee],h4eq,ee,str(-h),str(k))
    
    c = random.choice([-1,1])*random.randint(1,9)
    b = random.choice([-2,2])*random.randint(1,4)
    b4eq = num2sting(b)
    
    h = int(b/2)
    k = int(c+b^2+4)
    
    choice3_v2 = '$y=%s%sx-x^2$ จะได้กราฟของพาราโบลาคว่ำ และจุดยอดอยู่ที่ $(%s,%s)$'%(str(4),b4eq,str(h),str(k))
    v2 = [choice1_v2,choice2_v2,choice3_v2,choice4]
    
    my_random , ans8 = random_choice(random.choice([v1,v2]))
    choice = r'''
\begin{enumerate}[label={\arabic*.}]
\item {%s}
\item {%s}
\item {%s}
\item {%s}
\end{enumerate}
'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item8 += choice
    
    return item8 , str(ans8)

def gen_item_9() :
    item9 = '\item %s '%(all_test[8][1])
    a = random.choice(['','-'])
    b = random.choice([-2,2])*random.randint(1,4)
    c = random.choice([-1,1])*random.randint(1,9)
    b4eq = num2sting(b)
    c4eq = num2sting(c)
    item9 = item9%('$y=%sx^2%sx%s$'%(a,b4eq,c4eq))
    
    if a :
        k = int(c + b**2/4)
        choice1 = '$f$ มีค่าต่ำสุดเท่ากับ $%s$'%str(k)
        choice2 = '$f$ มีค่าต่ำสุดเท่ากับ $%s$'%str(-k)
        choice3 = '$f$ ไม่มีค่าสูงสุด'
        x = random.randint(1,8)
        y = x + 1
        num4check = math.sqrt(x/y)
        v = -(num4check)**2+b*num4check+c
        if v > 0 :
            choice4 = r'$f\left(\sqrt{\frac{%s}{%s}}\right) < 0$'%(str(x),str(y))
        else :
            choice4 = r'$f\left(\sqrt{\frac{%s}{%s}}\right) > 0$'%(str(x),str(y))
    else :
        k = int(c - b**2/4)
        choice1 = '$f$ มีค่าสูงสุดเท่ากับ $%s$'%str(k)
        choice2 = '$f$ มีค่าสูงสุดเท่ากับ $%s$'%str(-k)
        choice3 = '$f$ ไม่มีค่าต่ำสุด'
        x = random.randint(1,8)
        y = x + 1
        num4check = math.sqrt(x/y)
        v = (num4check)**2+b*num4check+c
        if v > 0 :
            choice4 = r'$f\left(\sqrt{\frac{%s}{%s}}\right) < 0$'%(str(x),str(y))
        else :
            choice4 = r'$f\left(\sqrt{\frac{%s}{%s}}\right) > 0$'%(str(x),str(y))
    
    my_random , ans9 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item9 += choice
    
    return item9 , str(ans9)

def gen_item_10() :
    item10 = '\item %s '%(all_test[9][1])
    x1 = random.choice([-1,1])*random.randint(1,20)
    newchoice_list = [e for e in range(-20,21) if (e != x1 and e%2 == x1%2)]
    x2 = random.choice(newchoice_list)
    item10 = item10%(x1,x2)
    
    ans = int((x1+x2)/2)
    choice1 = '$%s$'%str(ans)
    choice2 = '$%s$'%str(ans+1)
    choice3 = '$%s$'%str(ans+2)
    choice4 = '$%s$'%str(ans+3)

    my_random , ans10 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item10 += choice
    
    return item10 , str(ans10)

def gen_item_11() :
    item11 = '\item %s '%(all_test[10][1])
    b = random.choice([-2,2])*random.randint(1,4)
    c = random.randint(1,15)
    b4eq = num2sting(b)
    c4eq = num2sting(c)
    eq = '$y=-x^2%sx%s$'%(b4eq,c4eq)
    item11 = item11%eq
    
    h = int(b/2)
    k = int(c + b**2/4)
    
    choice1 = '$%s$'%str(h)
    choice2 = '$%s$'%str(-h)
    choice3 = '$%s$'%str(k)
    choice4 = '$%s$'%str(-k)
    
    my_random , ans11 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item11 += choice
    
    return item11,str(ans11)

def gen_item_12() :
    a = random.choice([2,4,6])
    b = random.choice([-3,-2,-1,1,2,3])*a
    c = random.randint(1,7)
    
    h = -b/(2*a)
    k = c - b**2/(4*a)
    
    q = 0
    if h>0 :
        if k>0 :
            q = 1
        elif k<0 :
            q = 4
    elif h<0 :
        if k>0 :
            q = 2
        elif k<0 :
            q = 3
    
    b4eq = num2sting(b)
    c4eq = num2sting(c)
    
    eq = '$f(x)=%sx^2%sx%s$'%(str(a),b4eq,c4eq)
    item12 = '\item พาราโบลารูปหนึ่งเป็นกราฟของสมการ %s จงพิจารณาข้อความต่อไปนี้'%(eq) + r'\\'
    
    h_x = random.choice([-3,-2,1,1,2,3])
    text2consider1 = 'ก. พาราโบลารูปนี้มีแกนสมมาตร คือ เส้นตรง $x=%d$'%h_x
    q_x = random.randint(1,4)
    text2consider2 = 'ข. พาราโบลามีจุดวกกลับอยู่ในจตุภาคที่ $%d$'%q_x
    
    item12 += text2consider1 + r'\\'
    item12 += text2consider2
    
    text1 = False
    text2 = False
    if h == h_x : 
        text1 = True
    if q == q_x :
        text2 = True
    
    posible_choice = ['ก. เท่านั้นที่เป็นจริง','ข. เท่านั้นที่เป็นจริง','ทั้ง ก. และ ข. เป็นจริง' , 'ทั้ง ก. และ ข. เป็นเท็จ']
    
    if text1 :
        if text2 :
            choice1 = 'ทั้ง ก. และ ข. เป็นจริง'
        else : 
            choice1 = 'ก. เท่านั้นที่เป็นจริง'
    else :
        if text2 :
            choice1 = 'ข. เท่านั้นที่เป็นจริง'
        else :
            choice1 = 'ทั้ง ก. และ ข. เป็นเท็จ'
    
    choice2,choice3,choice4 = [ch for ch in posible_choice if ch != choice1]
    
    my_random , ans12 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item12 += choice
    
    return item12 , str(ans12)

def gen_item_13() :
    item13 = '\item %s '%(all_test[12][1])
    a = random.choice(['','-'])
    b = random.choice([-6,-4,-2,2,4,6])
    k = random.choice([-1,1])*random.randint(1,5)
    
    
    b4eq = num2sting(b)
    
    eq = '$y=%sx^2%sx+c$'%(a,b4eq)
    
    item13 = item13%(eq,k)
    
    if a :
        a = -1
    else :
        a = 1
    
    c = int(k+a*(-b/(2*a))**2+b*(-b/(2*a)))
    c_fase2 = int(k-a*(-b/(2*a))**2+b*(-b/(2*a)))
    c_fase3 = int(k-a*(-b/(2*a))**2-b*(-b/(2*a)))
    c_fase4 = int(k+a*(-b/(2*a))**2-b*(-b/(2*a)))
    
    choice1 = '$%s$'%str(c)
    choice2 = '$%s$'%str(c_fase2)
    choice3 = '$%s$'%str(c_fase3)
    choice4 = '$%s$'%str(c_fase4)
    
    my_random , ans13 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item13 += choice
    
    return item13 , str(ans13)

def gen_item_14() :
    item14 = r'\item ถ้าเส้นตรง $x=3$ เป็นเส้นสมมาตรของกราฟของฟังก์ชัน $f(x)=-x^2+(k+5)x+(k^2-10)$ เมื่อ $k$ เป็นจำนวนจริง แล้ว $f$ มีค่าสูงสุดเท่ากับข้อใดต่อไปนี้'
    
    choice1 = '$0$'
    choice2 = '$-1$'
    choice3 = '$1$'
    choice4 = '$\frac{1}{2}$'
        
    my_random , ans14 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item14 += choice
    
    return item14 , ans14

def gen_item_15() :
    item15 = r'\item ถ้าต้นทุนในการผลิตของเล่น $x$ ชิ้น เท่ากับ $\frac{1}{3}x^2-20x-200$ บาท โดยขายของเล่นในราคาชิ้นละ $180$ บาท กำไรสูงสุดที่บริษัทนี้สามารถทำได้จากการขายสินค้าชิ้นนี้เท่ากับเท่าใด'
    
    choice1 = '$30,200$ บาท'
    choice2 = '$300$ บาท'
    choice3 = '$15,100$ บาท'
    choice4 = '$150$ บาท'
    
    my_random , ans15 = random_choice([choice1,choice2,choice3,choice4])
    choice = r'''\choice{%s}{%s}{%s}{%s}'''%(my_random[0],my_random[1],my_random[2],my_random[3]) + '\n'
    item15 += choice
    
    return item15 , ans15

if __name__ == '__main__' :
    # value4random = {'random1' : ['3','-3:3',random.choice(['1-x^2','-1+x^2'])],
    #                 'random2' : ['5','-3:3',random.choice(['4-x^2','-4+x^2'])],
    #                 'random3' : ['10','-4:4',random.choice(['9-x^2','-9+x^2'])],
    #                 'random4' : ['20','-6:6',random.choice(['16-x^2','-16+x^2'])],
    #                 'random5' : ['30','-6:6',random.choice(['25-x^2','-25+x^2'])],
    #                 'random6' : ['40','-8:8',random.choice(['36-x^2','-36+x^2'])]
    #                 }
    # x = random.choice([e for e in value4random.keys()])
    # print(x)
    
    f_test = envs.testfile
    get_all_test(f_test)
    
    print(gen_item_13())
    
    # x1 = random.choice([-1,1])*random.randint(1,20)
    # newchoice_list = [e for e in range(-20,21) if (e != x1)]
    # print(x1)
    # print(newchoice_list)
    # print(x1 in newchoice_list)
    
    # b = random.choice([-2,2])*random.randint(1,4)
    # print(b)
    
    # print(random.randint(1,10)**2)
    
    # count_item = list(range(1,3))
    # random.shuffle(count_item)
    # print(count_item)
    