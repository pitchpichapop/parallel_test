import os
import time
import glob

def zipmuliplefileby7ipcmd(zipout, attachs):
    import subprocess
    if os.path.exists(zipout):
        os.unlink(zipout)
    bin_ = r"\\mythswbk03ss103\RPA-Dev\7-Zip\7z.exe"
    cmd = '"%s" -mx5 a -tzip "%s" %s' % (bin_, zipout, ' '.join('"%s"' % e for e in attachs))
    print(len(cmd), cmd)
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = process.communicate()
    realo = (out + err).decode('utf-8')
    print(realo)
    time.sleep(2)
    return 'Everything is Ok' in realo and os.path.exists(zipout), realo

if __name__ == '__main__' :
    envs_latex = glob.glob(os.path.join(r"D:\parallel_test\test",'*'))
    
    latex_file = glob.glob(os.path.join(r"D:\parallel_test\4-7","4-7_test_*"))
    print(latex_file)
    # zipname = r"D:\parallel_test\4-7\4-7_test_2_62085.tex"
    # # print(envs_latex)
    # zipmuliplefileby7ipcmd(r'D:\parallel_test\x.zip',envs_latex +[zipname])