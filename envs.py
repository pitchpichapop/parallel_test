testfile = r'D:\parallel_test\quadratic_test.xlsx'
student_list = r'D:\parallel_test\student 4-7.xlsx'

header_of_latex = r'''
\documentclass[20pt,a4paper]{report}
\usepackage[a4paper, top=2.5cm, bottom=2.5cm, left=2cm, right=2cm]{geometry}
\usepackage{amsmath,esint,mathtools,eucal}
\usepackage[normalem]{ulem}
\usepackage{ifthen,mailmerge}
\usepackage{pgfplots}

%------------ ปรับสีลิงค์และ url -----------%/a
\usepackage{xcolor}

\usepackage{fancyhdr}

\fancyhf{}
\rhead{}
\lhead{}
\rfoot{}
\cfoot{\thepage}
%------------ ส่วนสำหรับการทำ choice ------------
\newcounter{choice}
\renewcommand\thechoice{\arabic{choice}}
\newcommand\choicelabel{\thechoice.}


        \newcommand{\fourch}[4]{
        \par
                \begin{tabular}{*{4}{@{}p{0.23\textwidth}}}
                1. ~#1 & 2. ~#2 & 3. ~#3 & 4. ~#4
                \end{tabular}
        }

        %(A)(B)
        %(C)(D)
        \newcommand{\twoch}[4]{

                \begin{tabular}{*{2}{@{}p{0.46\textwidth}}}
                1. ~#1 & 2. ~#2
                \end{tabular}
        \par
                \begin{tabular}{*{2}{@{}p{0.46\textwidth}}}
                3. ~#3 & 4. ~#4
                \end{tabular}
        }

        %(A)
        %(B)
        %(C)
        %(D)
        \newcommand{\onech}[4]{
        \par
              1. ~#1 \par 2. ~#2 \par 3. ~#3 \par 4. ~#4
        }

        \newlength\widthcha
        \newlength\widthchb
        \newlength\widthchc
        \newlength\widthchd
        \newlength\widthch
        \newlength\tabmaxwidth

        \setlength\tabmaxwidth{0.96\textwidth}
        \newlength\fourthtabwidth
        \setlength\fourthtabwidth{0.25\textwidth}
        \newlength\halftabwidth
        \setlength\halftabwidth{0.5\textwidth}

      \newcommand{\choice}[4]{%
      \settowidth\widthcha{AM.#1}\setlength{\widthch}{\widthcha}%
      \settowidth\widthchb{BM.#2}%
      \ifdim\widthch<\widthchb\relax\setlength{\widthch}{\widthchb}\fi%
      \settowidth\widthchb{CM.#3}%
      \ifdim\widthch<\widthchb\relax\setlength{\widthch}{\widthchb}\fi%
      \settowidth\widthchb{DM.#4}%
      \ifdim\widthch<\widthchb\relax\setlength{\widthch}{\widthchb}\fi%
      \ifdim\widthch<\fourthtabwidth
        \fourch{#1}{#2}{#3}{#4}
      \else\ifdim\widthch<\halftabwidth
        \ifdim\widthch>\fourthtabwidth
          \twoch{#1}{#2}{#3}{#4}
        \else
          \onech{#1}{#2}{#3}{#4}
        \fi
      \fi\fi
    }

%------------ color ------------%
\usepackage[unicode=true]{hyperref}
\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black},
}
\renewcommand\UrlFont{\normalfont}

\usepackage{dirtytalk}
\usepackage{enumitem}
\usepackage{pdfpages}

%------------ สำหรับการใช้ฟ้อนท์ภาษาไทย -----------%
\usepackage[no-math]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\XeTeXlinebreaklocale "th"
\renewcommand{\baselinestretch}{1.6}
\setmainfont[Script=Thai,Scale=1.4,ItalicFont={THSarabun Italic.ttf}, BoldFont={THSarabun Bold.ttf},BoldItalicFont={THSarabun Bold Italic.ttf} ]{THSarabun.ttf}
\setmonofont[Script=Thai,Scale=0.9]{DroidSansMono.ttf}



%--------------- เปลี่ยนชื่อรูปและตาราง -------------%
\usepackage[labelsep=space]{caption}
\renewcommand{\figurename}{รูปที่}  
\renewcommand{\tablename}{ตารางที่} 
%--------------- กำหนดรูปแบบการอ้างอิง -------------%
\usepackage[numbers,sort&compress]{natbib}
%-----------------------------------------------%

\usepackage{graphicx,amsmath,latexsym,amssymb,amsthm}


\newtheorem{thm}{ทฤษฎีบท}[section]
\newtheorem{cor}[thm]{บทแทรก}
\newtheorem{lem}[thm]{ทฤษฎีบทประกอบ}
\newtheorem{prop}[thm]{สมบัติที่}

\theoremstyle{definition}
\newtheorem{defn}[thm]{บทนิยาม}
\newtheorem{rem}[thm]{หมายเหตุ}
\newtheorem{ex}[thm]{ตัวอย่าง}
\numberwithin{equation}{section}

\DeclareMathOperator{\Var}{Var}
\DeclareMathOperator{\Ima}{Im}

\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\N}{\mathbb{N}}

\usepackage[dot]{dashundergaps}

\usepackage{chemfig}
%-----------------------------------------------%
%                     START                     %
%-----------------------------------------------%
'''

header_of_file = r'''
\begin{tabular*}{.90\textwidth}{@{\extracolsep{\fill}}lcr}


\textbf{แบบทดสอบรายวิชา ค31202} & 
\textbf{คณิตศาสตร์พื้นฐาน}
& \textbf{ชั้นมัธยมศึกษาปีที่ 3} \\

\textbf{ตอนที่ 1 แบบสอบปรนัย} & \textbf{จำนวน 10 ข้อ (1-10)} & \textbf{จำนวน \pageref{finalpage} หน้า} \\

\hline

\end{tabular*}
\begin{enumerate}
\begin{enumerate}[label=\textit{{\arabic*.}}]
\item \textit{เลือกคำตอบที่ถูกต้องที่สุดเพียงข้อเดียว โดยตอบลงในกระดาษคำตอบที่จัดเตรียมไว้ให้}
\item \textit{ห้ามนำแบบทดสอบออกนอกห้องสอบ}
\item \textit{ทุจริตการสอบถูกปรับตกในรายวิชานั้นและถูกลงโทษตามระเบียบโรงเรียน}
\end{enumerate}
\end{enumerate}

\begin{tabular}{l}

\hline
\textbf{\textit{ชื่อ 
\dotuline{\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\; %s \;\;\;\;\;\;\;\;\;\;\;\;\;}
ชั้น 
\dotuline{\;\;\;\; %s \;\;\;\;}
 เลขที่
 \dotuline{\;\;\;\; %s \;\;\;\;}
รหัสประจำตัว 
\dotuline{\;\;\;\; %s \;\;\;\;}
\\
\hline
 }} 
\end{tabular}
'''

