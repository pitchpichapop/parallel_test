def cv_(c):
    v = c.value
    if v is None:
        v = ''
    if type(v) == str:
        v = v.strip()
    if type(v) == float:
        v = round(v, 2)
    return v

def gettmpr(row):
    return [cv_(e) for e in row]