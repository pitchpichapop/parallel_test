import warnings
import time
import sys
import socket
import confs
import codecs
import envs
import random
import openpyxl
import os
import excels_utils
import shutil
import glob

def create_header(fname) :
    with open(fname,'w',encoding='utf-8') as f :
        f.write(envs.header_of_latex)

def begin_document(fname) :
    with open(fname,'a',encoding='utf-8') as f :
        f.write(r'\begin{document}' + '\n')

def end_document(fname) :
    with open(fname,'a',encoding='utf-8') as f :
        f.write(r'\label{finalpage}' + '\n')
        f.write(r'\end{document}' + '\n')

def get_file_header() :
    return envs.header_of_file

def write_file_header(fname,stdname,room,no,id) :
    with open(fname,'a',encoding='utf-8') as f :
        f_header = get_file_header()%(stdname,room,no,id)
        f.write(f_header)

def begin_test_item(fname) :
    with open(fname,'a',encoding='utf-8') as f :
        f.write(r'\begin{enumerate}' + '\n')

def end_test_item(fname) :
    with open(fname,'a',encoding='utf-8') as f :
        f.write(r'\end{enumerate}' + '\n')

all_ans = []
def gen_suffled_item(fname) :
    items = [1, 2, 6, 7, 8, 9, 10, 13, 14, 15]
    random.shuffle(items)
    with open(fname,'a',encoding='utf-8') as f :
        for s in items :
                item , ans = eval('confs.gen_item_'+str(s))()
                f.write(item)
                all_ans.append(ans)
                
def gen_all_test(std_data , stdroom) :
    root_path =  os.path.dirname(std_data)
    f_test = envs.testfile
    confs.get_all_test(f_test)
    std_master = os.path.join(root_path,'student %s.xlsx'%stdroom)
    wb = openpyxl.load_workbook(std_master,data_only=True)
    ws = wb['Sheet1']
    
    wb_ans = openpyxl.Workbook()
    ws_ans = wb_ans.worksheets[0]
    ws_ans.title = 'key'
        
    for i,row in enumerate(ws) : 
        if i==0 :
            continue
        row = excels_utils.gettmpr(row)
        # print(row)
        
        prefix , name , no , id , room = row[2] , row[3] , row[0] , row[1] , row[4]
        stdname = prefix + name
        f = '%s_test_%s_%s.tex'%(room.replace('/','-').replace('ม.',''),no,id)
        
        create_header(f)
    
        begin_document(f)
    
        write_file_header(f,stdname,str(room),str(no),str(id))
    
        begin_test_item(f)
        gen_suffled_item(f)
        end_test_item(f)
    
        end_document(f)
        
        
        f_origin = os.path.join(root_path,f)
        folder_des = os.path.join(root_path ,str(room).replace('/','-').replace('ม.','') , 'raw_main')
        confs.mkdir(folder_des)
        f_des = os.path.join(folder_des,f)
        shutil.copy2(f_origin,f_des)
        os.unlink(f_origin)
        
        ws_ans.append(row + all_ans)
        wb_ans.save(os.path.join(folder_des,'key_%s.xlsx'%str(room).replace('/','-').replace('ม.','')))
        all_ans.clear()
        
        # print(row + all_ans)
    ready_dir = os.path.join(root_path ,str(room).replace('/','-').replace('ม.','') , 'ready')
    confs.mkdir(ready_dir)
    envs_latex = glob.glob(os.path.join(r"D:\parallel_test\test",'*'))
    for latex_file in glob.glob(os.path.join(r"D:\parallel_test\%s"%stdroom, 'raw_main' , "%s_test_*"%stdroom)) :
        zipname = latex_file.replace('.tex','.zip')
        zipmuliplefileby7ipcmd(zipname,envs_latex +[latex_file])
        shutil.copy2(zipname,zipname.replace('raw_main','ready'))
        os.unlink(zipname)

def zipmuliplefileby7ipcmd(zipout, attachs):
    import subprocess
    if os.path.exists(zipout):
        os.unlink(zipout)
    bin_ = r"\\mythswbk03ss103\RPA-Dev\7-Zip\7z.exe"
    cmd = '"%s" -mx5 a -tzip "%s" %s' % (bin_, zipout, ' '.join('"%s"' % e for e in attachs))
    print(len(cmd), cmd)
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = process.communicate()
    realo = (out + err).decode('utf-8')
    print(realo)
    time.sleep(2)
    return 'Everything is Ok' in realo and os.path.exists(zipout), realo

if __name__ == '__main__' :
    print(__file__)
    stime = time.time()
    warnings.filterwarnings("ignore")
    sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
    print(sys.argv)
    print(socket.gethostname())
    
    if len(sys.argv) != 2 :
        print('usage:\tmain.py <student_data> <student room>')
        sys.exit(0)
    else :
        
        std_file = sys.argv[1]
        std_room = sys.argv[2]
        gen_all_test(std_file,std_room)
        
        # root_path = r'D:\parallel_test'
        # f_test = envs.testfile
        # confs.get_all_test(f_test)
        # std_master = os.path.join(r'D:\parallel_test','student 4-1.xlsx')
        # wb = openpyxl.load_workbook(std_master,data_only=True)
        # ws = wb['Sheet1']
        
        # wb_ans = openpyxl.Workbook()
        # ws_ans = wb_ans.worksheets[0]
        # ws_ans.title = 'key'
            
        # for i,row in enumerate(ws) : 
        #     if i==0 :
        #         continue
        #     row = excels_utils.gettmpr(row)
        #     # print(row)
            
        #     prefix , name , no , id , room = row[2] , row[3] , row[0] , row[1] , row[4]
        #     stdname = prefix + name
        #     f = '%s_test_%s_%s.tex'%(room.replace('/','-').replace('ม.',''),no,id)
            
        #     create_header(f)
        
        #     begin_document(f)
        
        #     write_file_header(f,stdname,str(room),str(no),str(id))
        
        #     begin_test_item(f)
        #     gen_suffled_item(f)
        #     end_test_item(f)
        
        #     end_document(f)
            
            
        #     f_origin = os.path.join(root_path,f)
        #     folder_des = os.path.join(root_path ,str(room).replace('/','-').replace('ม.','') , 'raw_main')
        #     confs.mkdir(folder_des)
        #     f_des = os.path.join(folder_des,f)
        #     shutil.copy2(f_origin,f_des)
        #     os.unlink(f_origin)
        #     "D:\parallel_test\test_1_62064.tex"
            
            
        #     ws_ans.append(row + all_ans)
        #     wb_ans.save(os.path.join(folder_des,'key_%s.xlsx'%str(room).replace('/','-').replace('ม.','')))
        #     all_ans.clear()
            
        #     # print(row + all_ans)
        # ready_dir = os.path.join(root_path ,str(room).replace('/','-').replace('ม.','') , 'ready')
        # confs.mkdir(ready_dir)
        # envs_latex = glob.glob(os.path.join(r"D:\parallel_test\test",'*'))
        # for latex_file in glob.glob(os.path.join(r"D:\parallel_test\4-1", 'raw_main' , "4-1_test_*")) :
        #     zipname = latex_file.replace('.tex','.zip')
        #     zipmuliplefileby7ipcmd(zipname,envs_latex +[latex_file])
        #     shutil.copy2(zipname,zipname.replace('raw_main','ready'))
        #     os.unlink(zipname)